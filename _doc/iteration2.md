# Iteration 2

Additional requirements:

Now, the discounts are only applied if the specific number of books are a consecutive set.

So for example, the 5% discount would only apply if you bought book one and book two, or book 6 and book 7, and so fort.

Whereas the 20% discount would only apply if you bought book one, book two, book three and book 4 - you get the idea.
