# Potter Kata Template

## Introduction

### Iteration 1

Once upon a time there was a series of seven books about a very English hero called Harry. Children all over the world thought he was fantastic, and, of course, so did the publisher. So in a gesture of immense generosity to mankind, (and to increase sales) they set up the following pricing model to take advantage of Harry’s magical powers.

One copy of any of the seven books costs 8 EUR. If, however, you buy two different books from the series, you get a 5% discount on those two books. If you buy 3 different books, you get a 10% discount. With 4 different books, you get a 20% discount. If you go the whole hog, and buy all 7, you get a huge 25% discount.

Note that if you buy, say, four books, of which 3 are different titles, you get a 10% discount on the 3 that form part of a set, but the fourth book still costs 8 EUR.

Potter mania is sweeping the country and parents of teenagers everywhere are queueing up with shopping baskets overflowing with Potter books. Your mission is to write a piece of code to calculate the price of any conceivable shopping basket, giving as big a discount as possible.

For example, how much does this basket of books cost?

- 2 copies of the first book
- 2 copies of the second book
- 2 copies of the third book
- 1 copy of the fourth book
- 1 copy of the fifth book

Answer :

    (4 * 8) - 20% [first book, second book, third book, fourth book]
    + (4 * 8) - 20% [first book, second book, third book, fifth book]
    = 25.6 * 2
    = 51.20

### More

Should you feel like you need any additional clues, check out the file `iteration1_clues.md` in the `_doc` directory.

Further challenges can be found in the `_doc` directory (See file `iteration2.md`).
Please go through them step by step, and try not to read them all in advance to keep a fresh mind.

## Setup

This setup requires PHP `^7.3` if you do not have it installed locally, you can use docker instead.

### Docker Usage

To run composer install:

`$ docker run --rm --interactive --tty --volume $PWD:/app composer install`

To create an interactive docker container, run:

`$ docker run -it --rm --name fizz-buzz-kata -v "$PWD":/app -w /app php:7.3-cli bash`

Or to run the test suite:

`$ docker run --rm -v "$PWD":/app -w /app php:7.3-cli php ./vendor/bin/phpunit`


## Credit

See the original kata at [Coding Dojo](http://codingdojo.org/kata/Potter/)
